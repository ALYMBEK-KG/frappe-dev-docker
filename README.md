# frappe-dev-docker
[Frappe framework docs](https://frappeframework.com/docs/user/en/basics)

## Getting started

At first install Dev Containers for vscode.
Then `Reopen in Container` ([more info](https://github.com/frappe/frappe_docker/blob/main/docs/development.md))

Inside a container run command: `python installer.py`

After installation is complete run following commands:

```
cd frappe-bench
bench start
```

Your website will now be accessible at location http://development.localhost:8000
